describe('Protractor Demo App', function() {
  beforeEach(function () {
    browser.ignoreSynchronization = true;
  });
  it('should have a title', function() {
    browser.get('http://localhost:7777/');

    expect(browser.getTitle()).toEqual('React Router Tutorial');
  });
});